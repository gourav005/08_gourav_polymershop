import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MensoutwearComponent } from './mensoutwear/mensoutwear.component';
import {MatTabsModule} from '@angular/material/tabs';
import { CheckoutComponent } from './checkout/checkout.component';
import { ItemdetailsComponent } from './itemdetails/itemdetails.component';

@NgModule({
  declarations: [
    AppComponent,
    MensoutwearComponent,
    CheckoutComponent,
    ItemdetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatTabsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
